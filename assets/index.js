document.addEventListener('DOMContentLoaded', () => {
    const items = ['gunting', 'batu', 'kertas'];
    const guntingPlayer = document.querySelector('#gunting-player');
    const batuPlayer = document.querySelector('#batu-player');
    const kertasPlayer = document.querySelector('#kertas-player');
    const guntingCom = document.querySelector('#gunting-com');
    const batuCom = document.querySelector('#batu-com');
    const kertasCom = document.querySelector('#kertas-com');
    const box = document.querySelector('.box');
    const message = document.querySelector('.message');
    const refresh = document.querySelector('#refresh');

    guntingPlayer.addEventListener('click', () => check('gunting'));
    batuPlayer.addEventListener('click', () => check('batu'));
    kertasPlayer.addEventListener('click', () => check('kertas'));
    refresh.addEventListener('click', () => {
        selectedPlayer.style.backgroundColor = null;
        selectedPlayer.style.borderRadius = null;
        selectedCom.style.backgroundColor = null;
        selectedCom.style.borderRadius = null;
        box.style.display = 'none';
    });

    function check(itemClicked) {
        let itemCom = items[Math.floor(Math.random() * items.length)];
        
        console.log(itemClicked);
        if (itemClicked === 'gunting' && itemCom === 'kertas' || itemClicked === 'batu' && itemCom === 'gunting' || itemClicked === 'kertas' && itemCom === 'batu') {
            console.log('player win');
            message.innerHTML = 'PLAYER WIN';
            box.style.backgroundColor = '#4C9654';
        } else if (itemClicked === 'gunting' && itemCom === 'batu' || itemClicked === 'batu' && itemCom === 'kertas' || itemClicked === 'kertas' && itemCom === 'gunting') {
            console.log('com win');
            message.innerHTML = 'COM WIN';
            box.style.backgroundColor = '#4C9654';
        } else {
            console.log('draw');
            message.innerHTML = 'DRAW';
            box.style.backgroundColor = '#035B0C';
        }

        if (itemClicked === 'gunting') {
            selectedPlayer = guntingPlayer;
        } else if (itemClicked === 'batu') {
            selectedPlayer = batuPlayer;
        } else {
            selectedPlayer = kertasPlayer;
        }

        if (itemCom === 'gunting') {
            selectedCom = guntingCom;
        } else if (itemCom === 'batu') {
            selectedCom = batuCom;
        } else {
            selectedCom = kertasCom;
        }

        selectedPlayer.style.backgroundColor = '#C4C4C4';
        selectedPlayer.style.borderRadius = '20px';
        selectedCom.style.backgroundColor = '#C4C4C4';
        selectedCom.style.borderRadius = '20px';
        box.style.display = 'block';
    }
});
